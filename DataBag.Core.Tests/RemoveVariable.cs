﻿namespace DataBag.Core.Tests
{
    using System;
    using NUnit.Framework;

    public class RemoveVariable
    {
        [Test]
        public void RemoveRegisteredVariable()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();

            Assert.That(dataBag.IsRegistered("var1"), Is.False);
            dataBag.Register("var1", "var1Value1");
            dataBag.Register("var1", "var1Value2");
            Assert.That(dataBag.IsRegistered("var1"), Is.True);

            dataBag.Remove("var1");
            Assert.That(dataBag.IsRegistered("var"), Is.False);
        }

        [Test]
        public void RemoveNotRegisteredVariable()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();

            Assert.Throws<DataBagException>(() =>
            {
                dataBag.Remove("var1");
            });
        }

        [Test]
        public void RemoveAndRestorePoint()
        {
            IDataBag dataBag = DataBagFactory.GetInstance();
            dataBag.Register("var1", "var1Value1");
            dataBag.CreateBackup("test");

            dataBag.Remove("var1");
            Assert.That(dataBag.IsRegistered("var1"), Is.False);

            dataBag.RestoreBackup("test");
            Assert.That(dataBag.IsRegistered("var1"), Is.True);

            dataBag.Remove("var1");
            Assert.That(dataBag.IsRegistered("var1"), Is.False);
        }
    }
}
