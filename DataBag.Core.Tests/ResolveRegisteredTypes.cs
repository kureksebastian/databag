﻿namespace DataBag.Core.Tests
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ResolveRegisteredTypes
    {
        [Test]
        public void Null()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("null", null);
            var a = bag.Resolve("[null]");
            Assert.That(a, Is.Null);
        }

        [TestCase("number", typeof(int), 12)]
        [TestCase("boolean", typeof(bool), false)]
        [TestCase("text", typeof(string), "my expression")]
        public void ResolveSingleVariable(string name, Type expectedType, object expression)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register(name, expression);
            var value = bag.Resolve($"[{name}]");
            Assert.That(value.GetType(), Is.EqualTo(expectedType));
            Assert.That(value, Is.EqualTo(expression));
        }

        [TestCase("one variable [var1]", "one variable var1Value")]
        [TestCase("[var1] and [var2] and [var3] test", "var1Value and var2Value and [var3] test")]
        [TestCase("[var1] ", "var1Value ")]
        [TestCase("null test [var_null]", "null test ")]
        [TestCase("null test [var_null] and [var_null]", "null test  and ")]
        public void ResolveExpression(string expression, string expectedValue)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("var1", "var1Value");
            bag.Register("var2", "var2Value");
            bag.Register("var_null", null);

            var value = bag.Resolve(expression);
            Assert.That(value.GetType(), Is.EqualTo(typeof(string)));
            Assert.That(value, Is.EqualTo(expectedValue));
        }

        [TestCase("[var3]", "var1Value")]
        [TestCase("[var2]", "var2Value and var1Value nested")]
        [TestCase("test [var1] and [var2]", "test var1Value and var2Value and var1Value nested")]
        public void ResolveExpressionWithNestedVariables(string expression, string expectedValue)
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("var1", "var1Value");
            bag.Register("var2", "var2Value and [var1] nested");
            bag.Register("var3", "[var1]");

            var value = bag.Resolve(expression);
            Assert.That(value.GetType(), Is.EqualTo(typeof(string)));
            Assert.That(value, Is.EqualTo(expectedValue));
        }

        [Test]
        public void GetSingleVariable()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("var1", "var1Value");
            bag.Register("var2", "var2Value and [var1] nested");

            Assert.That(
                bag.Get("var2"),
                Is.EqualTo("var2Value and var1Value nested"));
        }

        [Test]
        public void GetSingleVariableNotFound()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("var1", "var1Value");

            Assert.Throws<DataBagException>(() =>
            {
                bag.Get("[var1]");
            });
        }

        [Test]
        public void ResolveDynamicVariable()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.RegisterDynamic("var1", () => "test");
            bag.RegisterDynamic("var2", () => "[var1]2");

            var value = bag.Resolve("[var2]");
            Assert.That(value.GetType(), Is.EqualTo(typeof(string)));
            Assert.That(value, Is.EqualTo("test2"));
        }
    }
}
