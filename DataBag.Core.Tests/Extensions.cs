﻿namespace DataBag.Core.Tests
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using NUnit.Framework;
    using Variables.Attributes;

    [TestFixture]
    public class Extensions
    {
        [Test]
        public void RegisterAndGetFromObject()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register<Employer>(e => e.Name, "TestEmployer")
               .Register<Employer>(e => e.Created, DateTime.Now)
               .Register<Employer>(e => e.Number, "10001")
               .RegisterDynamic<Employer>(e => e.Enabled, () => true);

            Assert.That(
                bag.Get<Employer>(e => e.Name),
                Is.EqualTo("TestEmployer"));

            Assert.That(
                bag.Get<Employer>(e => e.Enabled),
                Is.EqualTo(true));

            Assert.That(
                bag.Get<Employer>(e => e.Number),
                Is.EqualTo("10001"));

            Assert.That(
                bag.Get<Employer, int>(e => e.Number),
                Is.EqualTo(10001));
        }

        [Test]
        public void RegisterAndGetEntireObject()
        {
            IDataBag bag = DataBagFactory.GetInstance();

            bag.Register<Employer>(e => e.Name, "TestEmployer")
               .RegisterDynamic<Employer>(e => e.Number, () => 10001);

            var employer = bag.GetFor<Employer>();
            Assert.That(employer.Name, Is.EqualTo("TestEmployer"));
            Assert.That(employer.Number, Is.EqualTo("10001"));
            Assert.That(employer.Enabled, Is.EqualTo(default(bool)));
            Assert.That(employer.Created, Is.EqualTo(default(DateTime)));
        }

        [Test]
        public void IgnoreWhenResolvingClass()
        {
            IDataBag bag = DataBagFactory.GetInstance();

            bag.Register<Employer>(e => e.Name, "TestEmployer")
               .Register<Employer>(e => e.IgnoredWhenResolvingClass, "test");

            Assert.That(
                bag.Get<Employer>(e => e.IgnoredWhenResolvingClass),
                Is.EqualTo("test"));

            var employer = bag.GetFor<Employer>();
            Assert.That(employer.Name, Is.EqualTo("TestEmployer"));
            Assert.That(employer.IgnoredWhenResolvingClass, Is.EqualTo(default(string)));
        }

        [Test]
        public void CustomVariableName()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register<Employer>(e => e.Id, Guid.NewGuid())
               .Register<Employer>(e => e.Name, "TestEmployer")
               .Register<CommonVariables>(c => c.RandomIntNumber, 15)
               .Register<CommonVariables>(c => c.DayBeforeYesterday, DateTime.Today.AddDays(-2));

            // Default variable name
            Assert.That(bag.IsRegistered("Employer.Name"), Is.True);

            // Do not include class name in variables
            Assert.That(bag.IsRegistered("CommonVariables.DayBeforeYesterday"), Is.False);
            Assert.That(bag.IsRegistered("DayBeforeYesterday"), Is.True);

            // Custom variable name
            Assert.That(bag.IsRegistered("Employer.Id"), Is.False);
            Assert.That(bag.IsRegistered("CustomVariableNameForId"), Is.True);

            Assert.That(bag.IsRegistered("CommonVariables.RandomIntNumber"), Is.False);
            Assert.That(bag.IsRegistered("RandomIntNumber"), Is.False);
            Assert.That(bag.IsRegistered("random"), Is.True);
        }

        [Test]
        public void Restore()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register<Employer>(e => e.Name, "TestEmployer")
               .Register<Employer>(e => e.Name, "TestEmployerUpdated2")
               .Register<Employer>(e => e.Name, "TestEmployerUpdated3")
               .Restore<Employer>(e => e.Name);

            Assert.That(
                bag.Get<Employer>(e => e.Name),
                Is.EqualTo("TestEmployerUpdated2"));
        }

        [Test]
        public void Copy()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register<CommonVariables>(v => v.RandomIntNumber, 100)
               .Register<Employer>(e => e.Name, "TestEmployer");

            bag.Copy<CommonVariables, Employer>(
                v => v.RandomIntNumber, 
                e => e.Name);

            Assert.That(bag.Get<Employer>(e => e.Name), Is.EqualTo(100));
        }

        [Test]
        public void TempChangesViaTempVariable()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("p1", "p1Value");
            bag.Register("p1", "p1ValueUpdated");
            bag.Register("p2", "p2Value");
           
            using (var tempBag = bag.ForTempChanges())
            {
                tempBag.Restore("p1");
                tempBag.Remove("p2");
                tempBag.Register("p3", "p3Value");

                Assert.That(tempBag.Get("p1"), Is.EqualTo("p1Value"));
                Assert.That(tempBag.IsRegistered("p2"), Is.False);
            }

            Assert.That(bag.Get("p1"), Is.EqualTo("p1ValueUpdated"));
            Assert.That(bag.IsRegistered("p2"), Is.True);
            Assert.That(bag.IsRegistered("p3"), Is.False);
        }

        [Test]
        public void ShouldNotMakeTempChanges()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("p1", "p1Value");

            using (bag)
            {
                bag.Register("p1", "p1ValueUpdated");
            }

            Assert.That(bag.Get("p1"), Is.EqualTo("p1ValueUpdated"));
        }

        [Test]
        public void NestedTempChanges()
        {
            IDataBag bag = DataBagFactory.GetInstance();
            bag.Register("p1", 1);

            using (bag.ForTempChanges())
            {
                bag.Register("p1", 2);
                bag.Register("p1", 3);

                using (bag.ForTempChanges()
                    .Register("p1", 4)
                    .Register("p1", 5))
                {
                    Assert.That(bag.Get("p1"), Is.EqualTo(5));

                    bag.Register("p1", 6);
                }

                Assert.That(bag.Get("p1"), Is.EqualTo(3));

                using (bag.ForTempChanges())
                {
                    bag.Register("p1", 7);
                }

                Assert.That(bag.Get("p1"), Is.EqualTo(3));
            }

            Assert.That(bag.Get("p1"), Is.EqualTo(1));
        }
        
        [SkipClassNameInVariables]
        public class CommonVariables
        {
            public DateTime DayBeforeYesterday { get; set; }

            [VariableName("random")]
            public int RandomIntNumber { get; set; }
        }

        public class Employer
        {
            [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Some fields must be public")]
            public bool Enabled;

            [VariableName("CustomVariableNameForId")]
            public Guid Id { get; set; }

            public string Name { get; set; }

            public DateTime Created { get; set; }

            public string Number { get; set; }

            [IgnoreWhenResolvingClass]
            public string IgnoredWhenResolvingClass { get; set; }
        }
    }
}
