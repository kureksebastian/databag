﻿namespace DataBag.Core.Functions.BuiltIn
{
    public class IntFunctionFactory
    {
        public static object Add(int input, string[] parameters)
        {
            return input + int.Parse(parameters[0]);
        }

        public static object IsEqualTo(int input, string[] parameters)
        {
            return input == int.Parse(parameters[0]);
        }

        public static object IsGreaterThan(int input, string[] parameters)
        {
            return input > int.Parse(parameters[0]);
        }

        public static object IsEqualToOrGreaterThan(int input, string[] parameters)
        {
            return input >= int.Parse(parameters[0]);
        }

        public static object IsLessThan(int input, string[] parameters)
        {
            return input < int.Parse(parameters[0]);
        }

        public static object IsEqualToOrLessThan(int input, string[] parameters)
        {
            return input <= int.Parse(parameters[0]);
        }
    }
}