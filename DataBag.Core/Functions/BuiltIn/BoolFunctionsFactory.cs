﻿namespace DataBag.Core.Functions.BuiltIn
{
    public class BoolFunctionsFactory
    {
        public static object Then(bool input, string[] parameters)
        {
            return input
                ? parameters[0]
                : parameters[1];
        }
    }
}