﻿namespace DataBag.Core.Functions
{
    using System;

    public interface IFunction
    {
        string Name { get; }

        Type InputType { get; }

        object Execute(object input, params string[] parameters);
    }
}
