﻿namespace DataBag.Core
{
    using System;

    public class DataBagException : Exception
    {
        public DataBagException(string message)
            : base(message)
        {
        }
    }
}
