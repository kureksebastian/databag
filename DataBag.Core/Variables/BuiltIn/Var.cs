﻿namespace DataBag.Core
{
    using System;
    using Variables.Attributes;

    [SkipClassNameInVariables]
    public class Var : IRegisterVariables
    {
        public string Null { get; set; }

        public string Guid { get; set; }

        public string String { get; set; }

        public string Int { get; set; }

        public string Now { get; set; }

        public string Today { get; set; }

        public string Yesterday { get; set; }

        public string Tomorrow { get; set; }

        public void Register(IDataBag dataBag)
        {
            dataBag
                .Register<Var>(v => v.Null, null)
                .Register<Var>(v => v.String, string.Empty)
                .Register<Var>(v => v.Int, 0)
                .RegisterDynamic<Var>(v => v.Guid, () => System.Guid.NewGuid())

                .RegisterDynamic<Var>(v => v.Now, () => DateTime.Now)
                .RegisterDynamic<Var>(v => v.Today, () => DateTime.Today)
                .RegisterDynamic<Var>(v => v.Yesterday, () => DateTime.Today.AddDays(-1))
                .RegisterDynamic<Var>(v => v.Tomorrow, () => DateTime.Today.AddDays(1));
        }
    }
}
