﻿namespace DataBag.Core.Variables
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Settings;

    public class VariableManager
    {
        private Dictionary<string, Stack<IValueProvider>> variables;
        private Dictionary<string, Dictionary<string, Stack<IValueProvider>>> backups;
        private ISettings settings;

        public VariableManager(ISettings settings)
        {
            variables = new Dictionary<string, Stack<IValueProvider>>();
            backups = new Dictionary<string, Dictionary<string, Stack<IValueProvider>>>();
            this.settings = settings;
        }

        public void Add(string name, IValueProvider valueProvider)
        {
            Validate(name, valueProvider);

            var variableName = GetVariableName(name);
            if (!variables.ContainsKey(variableName))
            {
                variables.Add(variableName, new Stack<IValueProvider>());
            }

            variables[variableName].Push(valueProvider);
        }

        public void Restore(string name)
        {
            var variableName = GetVariableName(name);

            if (!variables.ContainsKey(variableName))
            {
                throw new DataBagException($"Cannot restore undefined variable '{name}'");
            }

            var variableStack = variables[variableName];
            if (variableStack.Count <= 1)
            {
                throw new DataBagException($"Cannot restore '{name}', because the variable has not been updated");
            }

            variableStack.Pop();
        }

        public void Remove(string name)
        {
            var variableName = GetVariableName(name);

            if (!variables.ContainsKey(variableName))
            {
                throw new DataBagException($"Cannot remove variable '{name}' which does not exist");
            }

            variables.Remove(variableName);
        }

        public IEnumerable<string> GetNames()
        {
            return variables.Keys.AsEnumerable();
        }

        public bool Exists(string name)
        {
            var variableName = GetVariableName(name);
            return variables.ContainsKey(variableName);
        }

        public void CreateBackup(string name)
        {
            backups[name] = CreateCopy(variables);
        }

        public void RestoreBackup(string name)
        {
            if (!backups.ContainsKey(name))
            {
                throw new DataBagException($"Cannot find backup '{name}'");
            }

            var backup = backups[name];
            variables = CreateCopy(backup);
        }

        public void RemoveBackup(string name)
        {
            if (!backups.ContainsKey(name))
            {
                throw new DataBagException($"Cannot find backup '{name}'");
            }

            backups.Remove(name);
        }

        public IValueProvider GetOrNull(string name)
        {
            var variableName = GetVariableName(name);

            return variables.ContainsKey(variableName)
                ? variables[variableName].Peek()
                : null;
        }

        private void Validate(string name, IValueProvider valueProvider)
        {
            if (valueProvider == null)
            {
                throw new DataBagException("Value provider cannot be null");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new DataBagException("Variable name cannot be empty");
            }

            if (!GetNameValidator().IsMatch(name))
            {
                throw new DataBagException("Incorrect variable name");
            }

            if (name.Contains(settings.FunctionSeparator.ToString()))
            {
                throw new DataBagException($"Incorrect variable name ('{settings.FunctionSeparator}' is no allowed)");
            }

            if (name.Contains(settings.FunctionParameterSeparator.ToString()))
            {
                throw new DataBagException($"Incorrect variable name ('{settings.FunctionParameterSeparator}' is no allowed)");
            }

            if (name.Contains(settings.StartVariableMarker))
            {
                throw new DataBagException($"Incorrect variable name ('{settings.StartVariableMarker}' is no allowed)");
            }

            if (name.Contains(settings.EndVariableMarker))
            {
                throw new DataBagException($"Invalid variable name ('{settings.EndVariableMarker}' is no allowed)");
            }
        }

        private Regex GetNameValidator()
        {
            return new Regex($"^{settings.VariableNameRegexPattern}$");
        }

        private string GetVariableName(string name)
        {
            return name.ToLower();
        }

        private Dictionary<string, Stack<IValueProvider>> CreateCopy(
            Dictionary<string, Stack<IValueProvider>> data)
        {
            return data.ToDictionary(
                    i => i.Key,
                    i => new Stack<IValueProvider>(i.Value.Reverse()));
        }
    }
}
