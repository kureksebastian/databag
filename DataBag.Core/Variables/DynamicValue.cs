﻿namespace DataBag.Core.Variables
{
    using System;

    public class DynamicValue : IValueProvider
    {
        private Func<object> getValue;

        public DynamicValue(Func<object> getValue)
        {
            if (getValue == null)
            {
                throw new DataBagException("Function cannot be null");
            }

            this.getValue = getValue;
        }

        public object GetValue()
        {
            return getValue();
        }
    }
}
