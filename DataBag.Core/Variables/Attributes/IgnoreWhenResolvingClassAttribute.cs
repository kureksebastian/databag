﻿namespace DataBag.Core.Variables.Attributes
{
    using System;
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class IgnoreWhenResolvingClassAttribute : Attribute
    {
    }
}
