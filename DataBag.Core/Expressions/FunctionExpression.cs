﻿namespace DataBag.Core.Expressions
{
    public class FunctionExpression
    {
        public string Name { get; set; }

        public string[] Parameters { get; set; }
    }
}
