﻿namespace DataBag.Core.Expressions
{
    using System.Collections.Generic;

    public class VariableExpression
    {
        public VariableExpression()
        {
            Functions = new List<FunctionExpression>();
        }

        public string Name { get; set; }

        public string Expression { get; set; }

        public List<FunctionExpression> Functions { get; set; }
    }
}
