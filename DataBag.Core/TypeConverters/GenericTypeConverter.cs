﻿namespace DataBag.Core.TypeConverters
{
    using System;

    public class GenericTypeConverter<TInput, TOutput> : ITypeConverter
    {
        private Func<TInput, TOutput> convert;

        private GenericTypeConverter()
        {
        }

        public Type InputType { get; private set; }

        public Type OutputType { get; private set; }

        public static GenericTypeConverter<TInput, TOutput> Create(Func<TInput, TOutput> converter)
        {
            if (converter == null)
            {
                throw new DataBagException("Converter cannot be null");
            }

            return new GenericTypeConverter<TInput, TOutput>()
            {
                InputType = typeof(TInput),
                OutputType = typeof(TOutput),
                convert = converter
            };
        }

        public object Convert(object input)
        {
            return convert((TInput)input);
        }
    }
}
