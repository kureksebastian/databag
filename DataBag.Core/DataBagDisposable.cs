﻿namespace DataBag.Core
{
    using System;
    using System.Collections.Generic;

    public class DataBagDisposable : IDataBag
    {
        private IDataBag dataBag;

        private string backupName;

        public DataBagDisposable(IDataBag dataBag)
        {
            this.dataBag = dataBag;
            backupName = Guid.NewGuid().ToString();
            this.dataBag.CreateBackup(backupName);
        }

        public void Dispose()
        {
            dataBag.RestoreBackup(backupName);
            dataBag.RemoveBackup(backupName);
        }

        public IDataBag Register(string name, object objOrExpression)
        {
            dataBag.Register(name, objOrExpression);
            return this;
        }

        public IDataBag RegisterDynamic(string name, Func<object> getObjOrExpressionFunc)
        {
            dataBag.RegisterDynamic(name, getObjOrExpressionFunc);
            return this;
        }

        public IDataBag Remove(string name)
        {
            dataBag.Remove(name);
            return this;
        }

        public IDataBag Restore(string name)
        {
            dataBag.Restore(name);
            return this;
        }

        public bool IsRegistered(string name)
        {
            return dataBag.IsRegistered(name);
        }

        public IEnumerable<string> GetNames()
        {
            return dataBag.GetNames();
        }

        public TOutput Get<TOutput>(string name)
        {
            return dataBag.Get<TOutput>(name);
        }

        public object Get(string name)
        {
            return dataBag.Get(name);
        }

        public object Get(string name, Type outputType)
        {
            return dataBag.Get(name, outputType);
        }

        public TOutput Resolve<TOutput>(object objOrExpression)
        {
            return dataBag.Resolve<TOutput>(objOrExpression);
        }

        public object Resolve(object objOrExpression)
        {
            return dataBag.Resolve(objOrExpression);
        }

        public object Resolve(object objOrExpression, Type outputType)
        {
            return dataBag.Resolve(objOrExpression, outputType);
        }

        public IDataBag CreateBackup(string name)
        {
            dataBag.CreateBackup(name);
            return this;
        }

        public IDataBag RestoreBackup(string name)
        {
            dataBag.RestoreBackup(name);
            return this;
        }

        public IDataBag RemoveBackup(string name)
        {
            dataBag.RemoveBackup(name);
            return this;
        }

        public IDataBag DefineTypeConverter<TInput, TOutput>(Func<TInput, TOutput> converter)
        {
            dataBag.DefineTypeConverter<TInput, TOutput>(converter);
            return this;
        }

        public IDataBag DefineFunction<TInput>(string name, Func<TInput, string[], object> func)
        {
            dataBag.DefineFunction<TInput>(name, func);
            return this;
        }
    }
}
