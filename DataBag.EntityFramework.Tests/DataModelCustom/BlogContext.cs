﻿namespace DataBag.EntityFramework.Tests.DataModelCustom
{
    using System.Data.Entity;

    public partial class BlogContext : DbContext
    {
        public BlogContext()
            : base("name=BloggingContext")
        {
        }

        public virtual DbSet<Blog> Blogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
