﻿namespace DataBag.EntityFramework.Tests
{
    using DataBag.Core;
    using DataBag.EntityFramework.Tests.DataModel;
    using NUnit.Framework;

    public class BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            var context = new BloggingContext();
            context.Database.ExecuteSqlCommand("DELETE FROM Posts");
            context.Database.ExecuteSqlCommand("DELETE FROM Blogs");
        }

        public IDataBag GetDataBag()
        {
            return DataBagFactory
                .GetInstance()
                .RegisterVariablesFromAssembly(typeof(NoForeignKeysTests).Assembly);
        }
    }
}
