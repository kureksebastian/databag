﻿namespace DataBag.EntityFramework
{
    using System;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FindByAttribute : Attribute
    {
    }
}
