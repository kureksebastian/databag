﻿namespace DataBag.EntityFramework.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DBVariableAttribute : Attribute
    {
        public bool OverrideIfAlreadyRegistered { get; set; } = false;
    }
}
