﻿namespace DataBag.EntityFramework
{
    using System;

    public class DataBagEFException : Exception
    {
        public DataBagEFException(string message)
            : base(message)
        {
        }
    }
}
